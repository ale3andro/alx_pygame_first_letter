import pygame

class Enemy:
    def __init__(self, name, image, x=100, y=100):
        self.name = name
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.x = self.x
        self.rect.y = self.y
        
        print("Beware, a new enemy was born...")

    def getImage(self):
        return self.image

    def getRect(self):
        return self.rect
    
    def getPos(self):
        return [self.x, self.y]
    
    def setX(self, x):
        self.x = x
        self.rect.x = self.x
    
    def setY(self, y):
        self.y = y
        self.rect.y = self.y
    
    def hitme(self, message):
        print(self.name + " says: " + str(message))

