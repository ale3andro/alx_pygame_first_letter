#!/usr/bin/python
# -*- coding: utf-8 -*-

# https://realpython.com/pygame-a-primer/
# API DOCS https://www.pygame.org/docs/genindex.html


# Simple pygame program

# Import and initialize the pygame library
import pygame, os
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 50, 50)
GREEN = (50, 255, 50)
pygame.init()

work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)

# Set up the drawing window
screen = pygame.display.set_mode([1000, 600])
pygame.display.set_caption('App Title')


circle_x = 0
circle_y = 0


def render()    :
    # Fill the background with white
    screen.fill((255, 255, 255))
    # Draw a solid blue circle in the center
    green_rect = pygame.draw.rect(screen, GREEN, (250, 250, 100, 100))
    red_rect = pygame.draw.rect(screen, RED, (circle_x, circle_y, 100, 100))

    collide = green_rect.colliderect(red_rect)
    print(collide)

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN and event.key==pygame.K_UP:
            print('UP')
            circle_y -= 20
        if event.type == pygame.KEYDOWN and event.key==pygame.K_DOWN:
            print('DOWN')
            circle_y += 20
        if event.type == pygame.KEYDOWN and event.key==pygame.K_LEFT:
            print('LEFT')
            circle_x -= 20
        if event.type == pygame.KEYDOWN and event.key==pygame.K_RIGHT:
            print('RIGHT')
            circle_x += 20

            

    render()

    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()