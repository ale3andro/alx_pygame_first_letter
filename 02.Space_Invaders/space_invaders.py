from pdb import run
import sys, pygame,os
import random
pygame.init()

# Change the working directory in order to be able to load the assets (mainly images) without full path declaration
work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 50, 50)
GREEN = (50, 255, 50)

size = width, height = 800, 600
speed = [1.0, 1.0]
black = 0, 0, 0

screen = pygame.display.set_mode(size)

score = 0
score_game_over = 10
spaceship = pygame.image.load("img/spaceship.png")
spaceship_rect = spaceship.get_rect()
spaceship_rect.x = 600
spaceship_rect.y = 400

asteroid = {}
asteroid_rect = {}
asteroid_speed = {}
random.seed(5)
for i in range(5):
    asteroid[i] = pygame.image.load("img/asteroid.png")
    asteroid_rect[i] = asteroid[i].get_rect()
    asteroid_rect[i].x = 0
    asteroid_rect[i].y = random.randint(50, 750)
    asteroid_speed[i] = random.randint(3, 7)

font = pygame.font.Font('freesansbold.ttf', 32)

game_title = font.render('Space Invaders', True, GREEN)
score_title = font.render('Score: 0', True, GREEN)

running = True

while running:
    pygame.time.delay(50) 
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            running = False
        if event.type == pygame.KEYDOWN and event.key==pygame.K_q:
            running = False
       
    keys = pygame.key.get_pressed()
    if keys[pygame.K_UP] and spaceship_rect.y>0:
        spaceship_rect.y -= 10
    if keys[pygame.K_DOWN] and spaceship_rect.y<500:
        spaceship_rect.y += 10


    screen.fill(black)

    if (score<score_game_over):
        for i in range(5):
            asteroid_rect[i].x+=asteroid_speed[i]
            if (asteroid_rect[i].x>700):
                asteroid_rect[i].x = 0
                asteroid_rect[i].y = random.randint(50, 750)
            screen.blit(asteroid[i], asteroid_rect[i])
            if (asteroid_rect[i].colliderect(spaceship_rect)):
                asteroid_rect[i].y = random.randint(50, 750)
                asteroid_rect[i].x = 0
                asteroid_speed[i] = random.randint(3, 7)
                score += 1
                score_title = font.render('Score: ' + str(score), True, GREEN)
    else:
        game_over = pygame.image.load("img/gameover.png")
        screen.blit(game_over, (400, 300))
    
    screen.blit(spaceship, spaceship_rect)
    
    screen.blit(game_title, (150,20))
    screen.blit(score_title, (600, 20))
    pygame.display.flip()