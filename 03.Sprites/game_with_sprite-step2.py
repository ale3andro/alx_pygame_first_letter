import pygame, os
from PIL import Image

# Change the working directory in order to be able to load the assets (mainly images) without full path declaration
work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)

pygame.init()

def get_image(sheet, frame, width, height, scale, colour):
		image = pygame.Surface((width, height)).convert_alpha()
		image.blit(sheet, (0, 0), ((frame * width), 0, width, height))
		image = pygame.transform.scale(image, (width * scale, height * scale))
		image.set_colorkey(colour)

		return image

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Spritesheets')

sprite_sheet_image = pygame.image.load('spritesheets/fire_stick.png').convert_alpha()

BG = (50, 50, 50)
BLACK = (0, 0, 0)

frames = []
frames_number = 11
current_frame = 0
last_update = pygame.time.get_ticks()
animation_cooldown = 200

for i in range(frames_number):
	frames.append(get_image(sprite_sheet_image, i, 68, 299, 1, BLACK))
	
hero_frames = []
hero_file_images = os.listdir('spritesheets/01_idle_a')
hero_scale = 0.1
for i in range(len(hero_file_images)):
	#https://stackoverflow.com/questions/8032642/how-can-i-obtain-the-image-size-using-a-standard-python-class-without-using-an
	theImageFile = Image.open('spritesheets/01_idle_a/' + hero_file_images[i])
	theImagePygame = pygame.image.load('spritesheets/01_idle_a/' + hero_file_images[i])
	image = pygame.Surface((theImageFile.size[0], theImageFile.size[1])).convert_alpha()
	image.blit(theImagePygame, (0, 0))
	image = pygame.transform.scale(image, (theImageFile.size[0] * hero_scale, theImageFile.size[1] * hero_scale))
	image.set_colorkey(BLACK)
	hero_frames.append(image)	


run = True
while run:
	#update background
	screen.fill(BG)
	current_time = pygame.time.get_ticks()
	if (current_time - last_update >= animation_cooldown):
		current_frame+=1
		if (current_frame==frames_number):
			current_frame=0
		last_update = current_time
		
	#show frame image
	screen.blit(frames[current_frame], (0, 0))
	screen.blit(hero_frames[current_frame], (300,0))
	
	#event handler
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			run = False

	pygame.display.update()

pygame.quit()