from pdb import run
import sys, pygame,os
import random
pygame.init()

# Change the working directory in order to be able to load the assets (mainly images) without full path declaration
work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 50, 50)
GREEN = (50, 255, 50)

size = width, height = 800, 600
speed = [1.0, 1.0]
black = 0, 0, 0

screen = pygame.display.set_mode(size)

score = 0
score_game_over = 10
snowman = pygame.image.load("img/snowman_small.png")
snowman_rect = snowman.get_rect()
snowman_rect.x = 300
snowman_rect.y = 400

snowflake = {}
snowflake_rect = {}
snowflake_speed = {}
random.seed(5)
for i in range(5):
    snowflake[i] = pygame.image.load("img/snowflake50.png")
    snowflake_rect[i] = snowflake[i].get_rect()
    snowflake_rect[i].x = random.randint(50, 750)
    snowflake_rect[i].y = 0
    snowflake_speed[i] = random.randint(1, 4)

font = pygame.font.Font('freesansbold.ttf', 32)

game_title = font.render('Feed the snowman', True, GREEN)
score_title = font.render('Score: 0', True, GREEN)

running = True

while running:
    pygame.time.delay(50) 
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            running = False
        if event.type == pygame.KEYDOWN and event.key==pygame.K_q:
            running = False
       
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and snowman_rect.x>0:
        snowman_rect.x -= 10
    if keys[pygame.K_RIGHT] and snowman_rect.x<800:
        snowman_rect.x += 10


    screen.fill(black)

    if (score<score_game_over):
        for i in range(5):
            snowflake_rect[i].y+=snowflake_speed[i]
            screen.blit(snowflake[i], snowflake_rect[i])
            if (snowflake_rect[i].colliderect(snowman_rect)):
                snowflake_rect[i].x = random.randint(50, 750)
                snowflake_rect[i].y = 0
                snowflake_speed[i] = random.randint(1, 4)
                score += 1
                score_title = font.render('Score: ' + str(score), True, GREEN)
    else:
        game_over = pygame.image.load("img/gameover.png")
        screen.blit(game_over, (400, 300))
    
    screen.blit(snowman, snowman_rect)
    
    screen.blit(game_title, (150,20))
    screen.blit(score_title, (600, 20))
    pygame.display.flip()