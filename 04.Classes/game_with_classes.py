import pygame
import os
from enemy import Enemy
from myfunctions import *

pygame.init()

work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)

print(emvado_trigwnou(10,20))

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 50, 50)
GREEN = (50, 255, 50)

window_size = width, height = 800, 600
screen = pygame.display.set_mode(window_size)

enemy0 = Enemy("Jobs", "img/bad_guy0.png")
enemy0.hitme("alex")
enemy0.setX(350)
enemy0.setY(0)

enemy1 = Enemy("Gates", "img/bad_guy1.png", 100, 300)
enemy1.hitme("nick")
enemy1.setX(350)
enemy1.setY(350)


running = True

while running:
    screen.fill(BLACK)
    pygame.time.delay(50)
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            running = False
    keys = pygame.key.get_pressed()
    
    if keys[pygame.K_LEFT]:
        if (enemy0.getPos()[0]>10):
            enemy0.setX(enemy0.getPos()[0] - 10)
    if keys[pygame.K_a]:
        if (enemy1.getPos()[0]>10):
            enemy1.setX(enemy1.getPos()[0] - 10)
    if keys[pygame.K_RIGHT]:
        if (enemy0.getPos()[0]<750):
            enemy0.setX(enemy0.getPos()[0] + 10)
    if keys[pygame.K_d]:
        if (enemy1.getPos()[0]<750):
            enemy1.setX(enemy1.getPos()[0] + 10)
    
    screen.blit(enemy0.getImage(), enemy0.getRect())
    screen.blit(enemy1.getImage(), enemy1.getRect())
    pygame.display.flip()