import pygame, os

pygame.init()

def get_image(sheet, frame, width, height, scale, colour):
		image = pygame.Surface((width, height)).convert_alpha()
		image.blit(sheet, (0, 0), ((frame * width), 0, width, height))
		image = pygame.transform.scale(image, (width * scale, height * scale))
		image.set_colorkey(colour)

		return image

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Spritesheets')
work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)
sprite_sheet_image = pygame.image.load('spritesheets/fire_stick.png').convert_alpha()

BG = (50, 50, 50)
BLACK = (0, 0, 0)

frame_0 = get_image(sprite_sheet_image, 0, 68, 299, 1, BLACK)
frame_1 = get_image(sprite_sheet_image, 1, 68, 299, 1, BLACK)
frame_2 = get_image(sprite_sheet_image, 2, 68, 299, 1, BLACK)
frame_3 = get_image(sprite_sheet_image, 3, 68, 299, 1, BLACK)

run = True
while run:

	#update background
	screen.fill(BG)

	#show frame image
	screen.blit(frame_0, (0, 0))
	screen.blit(frame_1, (72, 0))
	screen.blit(frame_2, (150, 0))
	screen.blit(frame_3, (250, 0))

	#event handler
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			run = False

	pygame.display.update()

pygame.quit()