import sys, pygame, os
pygame.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 50, 50)
GREEN = (50, 255, 50)

size = width, height = 800, 600
speed = [1.0, 1.0]
black = 0, 0, 0

work_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(work_dir)
screen = pygame.display.set_mode(size)

animal = pygame.image.load("assets/Αετός.png")
animal = pygame.transform.scale(animal, (200,200))
animal.set_alpha(50)
animal_rect = animal.get_rect()

font = pygame.font.Font('freesansbold.ttf', 32)
text = font.render('THE GAME', True, GREEN, RED)
text_rect = text.get_rect()
text_rect.x = 400
text_rect.y = 300


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    animal_rect = animal_rect.move(speed)
    if animal_rect.left < 0 or animal_rect.right > width:
        speed[0] = -speed[0]
    if animal_rect.top < 0 or animal_rect.bottom > height:
        speed[1] = -speed[1]

    screen.fill(black)
    screen.blit(animal, animal_rect)
    screen.blit(text, text_rect)
    pygame.display.flip()